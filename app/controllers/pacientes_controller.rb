class PacientesController < ApplicationController
    def nuevoPaciente
        @paciente=Paciente.new
        @paciente.nombre=params[:nombre]
        @paciente.save
        render "Nuevo paciente"
    end
    def nuevaCita
        @paciente=Paciente.find(params[:idPaciente])
        @medico=Medico.find(params[:idMedico])
        @cita=Cita.new
        @cita.paciente_id=@paciente.id
        @cita.medico_id= @medico.id
        @paciente.citas<<@cita
        @paciente.save
        render "Nueva cita"
    end
end
