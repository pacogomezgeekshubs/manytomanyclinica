class Medico < ApplicationRecord
    has_many :citas
    has_many :pacientes, through: :citas
end
