class Paciente < ApplicationRecord
    has_many :citas
    has_many :medicos, through: :citas
end
