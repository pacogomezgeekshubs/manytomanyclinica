class CreateCita < ActiveRecord::Migration[6.0]
  def change
    create_table :cita do |t|
      t.belongs_to :medico
      t.belongs_to :paciente
      t.datetime :fecha_cita
      t.timestamps
    end
  end
end
