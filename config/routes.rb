Rails.application.routes.draw do
resources :medicos, :pacientes
get "/medicos/nuevoMedico/:nombre", to: "medicos#nuevoMedico", as:"nuevoMedico"
get "/pacientes/nuevoPaciente/:nombre", to: "pacientes#nuevoPaciente", as:"nuevoPaciente"
get "/pacientes/nuevaCita/:idMedico/:idPaciente", to: "pacientes#nuevaCita", as:"nuevaCita"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
